from PyQt5.QtCore import QThread

class MyThread(QThread):

    def __init__(self):
        super(MyThread, self).__init__()

    # override run function
    def run(self):
        print(f'hello all, this th is {int(QThread.currentThreadId())}')


if __name__ == "__main__":

    th = MyThread()

    print(f'hello all, this th is {int(QThread.currentThreadId())}')

    th.start() # 執行執行序內工作
    th.wait()  # 等待執行序執行工作完成