import sys
from PyQt5.QtWidgets import QMainWindow, QApplication
from pyqtapp.ui_mainwindow import Ui_MainWindow

class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.textBrowser.append('Hello World')
        self.pushButton_A.clicked.connect(self.btnAfun)
        self.pushButton_B.clicked.connect(self.btnBfun)
        # 按鍵在qtcreator內編號可能不同，請先確認，並於此處修改

    def btnAfun(self):
        print('stdout btnA')
        self.textBrowser.append('push btn A')

    def btnBfun(self):
        self.textBrowser.append('push btn B')

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())